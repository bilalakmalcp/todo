import { Component } from '@angular/core';

@Component({
    selector: 'page_not_found',
    templateUrl: './page_not_found.html',
    styleUrls: ['./app.component.css']
})
export class PageNotFound {
}
