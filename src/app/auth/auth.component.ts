import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class Auth implements OnInit {

  /**
   * Variables
   */
  public auth_toggle: boolean = false;
  private user: firebase.User;
  private items: FirebaseListObservable<any[]>;
  private msgVal: string = '';
  private login_error: boolean;
  private register_error: boolean;
  private request_wait: boolean;

  constructor(public afAuth: AngularFireAuth, private router: Router) {
    this.login_error = false;
    this.register_error = false;
    this.request_wait = false;
  }

  ngOnInit(): void {
    this.afAuth.authState.subscribe(
      (auth) => {
        this.user = auth;
        if (this.user)
          this.router.navigate(['/tasks']);
      });
  }

  toggle_auth(flag) {
    this.auth_toggle = flag;
  }

  /**
   * Login Submit
   * @param login_form 
   */
  onLoginSubmit(login_form: NgForm) {
    if (login_form.valid) {
      this.request_wait = true;
      this.login_error = false;
      this.afAuth.auth.signInWithEmailAndPassword(login_form.value.email, login_form.value.password)
        .then((res) => {
          this.request_wait = false;
        })
        .catch(function (error) {
          this.request_wait = false;
          this.login_error = true;
        });
    } else {
      this.request_wait = false;
      this.login_error = true;
    }
  }

  /**
   * SignUp Submit
   * @param login_form 
   */
  onSignUpSubmit(registerForm: NgForm) {
    var that = this;
    if (registerForm.valid) {
      this.register_error = false;
      this.request_wait = true;
      this.afAuth.auth.createUserWithEmailAndPassword(registerForm.value.email, registerForm.value.password)
        .then((res) => {
          that.afAuth.auth.currentUser.updateProfile({
            displayName: registerForm.value.fullName,
            photoURL: ''
          }).then((res) => {
            this.request_wait = false;
            this.router.navigate(['/tasks']);
          });
        })
        .catch(function (error) {
          this.request_wait = false;
          that.register_error = true;
        });
    } else {
      this.request_wait = false;
      that.register_error = true;
    }
  }

  logout() {
    this.afAuth.auth.signOut();
  }
}
